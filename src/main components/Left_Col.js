const Left_Col = ()=>{
    return(
        /**
         * due to lack of time and motivation, admin panel is unfinished
         * -Christopher Hu
         */
        <section id="left-col">
            <section id="admin-panel">
                    <p>ADMIN</p>
                    <button className="adminBtn">Create Category</button>
                    <button className="adminBtn">Create Topic</button>
                    <button className="adminBtn">Close Topic</button>
                    <button className="adminBtn">Delete Topic</button>
            </section>
        </section>
    );
}

export default Left_Col;